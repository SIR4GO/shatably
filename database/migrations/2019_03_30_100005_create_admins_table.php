<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            //$table->string("username")->nullable();
            $table->string("password")->default(123);
            $table->string("email");
            $table->string("name");
            $table->boolean('login')->default(0);
           // $table->string("last_name");
            //$table->string("photo")->nullable();
            $table->string("phone_number")->nullable();
            $table->string("address")->nullable();
            $table->timestamps();
            $table->mediumText('image')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
