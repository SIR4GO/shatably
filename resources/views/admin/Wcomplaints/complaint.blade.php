@extends('layouts.app')


@section('content')
    <div class="card">
        <div class="card-header">
            View Complaint details
        </div>
        <div class="card-body">
            {{$details->comp_description}}<br>
            Compilant: {{$details->complainant}}<br>
            <a href="{{route('worker.show',['id'=>$details->worker_id])}}" class="float-left btn btn-info">Show Complained worker</a>
        </div>
    </div>
@endsection